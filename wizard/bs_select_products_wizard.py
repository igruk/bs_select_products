from odoo import fields, models, api


class BsSelectProducts(models.TransientModel):
    _name = 'bs.select.products'
    _description = 'Select Products'

    product_category_id = fields.Many2one('product.category', string='Product Category')
    order_lines = fields.One2many('bs.select.products.line', 'wizard_id', string='Order Lines')
    selected_lines = fields.One2many('bs.select.products.selected.line', 'wizard_id', string='Selected Lines')
    product_ids = fields.Many2many('product.product', string='Products')
    flag_order = fields.Char('Flag Order')

    @api.onchange('product_category_id')
    def _onchange_product_category_id(self):
        if self.product_category_id:
            products = self.env['product.product'].search([('categ_id', '=', self.product_category_id.id)], order='name')
            order_lines = [(0, 0, {
                'product_id': product.id,
                'price': product.lst_price,
                'quantity': 1.0,
                'product_uom': product.uom_id.id
            }) for product in products]
            self.order_lines = False
            self.order_lines = order_lines

    def select_products(self):
        order_id = self.env['sale.order'].browse(self._context.get('active_id', False))
        for product in self.selected_lines:
            self.env['sale.order.line'].create({
                'product_id': product.product_id,
                'product_uom': product.product_uom,
                'price_unit': product.product_id.lst_price,
                'order_id': order_id.id
            })


class BsSelectProductsLine(models.TransientModel):
    _name = 'bs.select.products.line'
    _description = 'Select Products Line'

    wizard_id = fields.Many2one('bs.select.products', string='Wizard')
    product_id = fields.Many2one('product.product', string='Product')
    price = fields.Float(string='Price')
    quantity = fields.Float(string='Quantity')
    product_uom = fields.Many2one('uom.uom', string='UoM')

    @api.onchange('quantity')
    def _onchange_quantity(self):
        selected_lines = {
            'category_id': self.product_id.categ_id.id,
            'product_id': self.product_id.id,
            'quantity': self.quantity,
            'product_uom': self.product_uom.id
        }
        # BsSelectProducts.selected_lines = [(0, 0, selected_lines)]
        BsSelectProducts.selected_lines = self.env['bs.select.products.selected.line'].create(selected_lines)


class BsSelectProductsSelectedLine(models.TransientModel):
    _name = 'bs.select.products.selected.line'
    _description = 'Selected Products Line'

    wizard_id = fields.Many2one('bs.select.products', string='Wizard')
    category_id = fields.Many2one('product.category', string='Category')
    product_id = fields.Many2one('product.product', string='Product')
    quantity = fields.Float(string='Quantity')
    product_uom = fields.Many2one('uom.uom', string='UoM')
